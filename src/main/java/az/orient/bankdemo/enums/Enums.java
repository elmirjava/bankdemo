package az.orient.bankdemo.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@Getter
public enum Enums {
    ACTIVE(1) , DEACTIVE(0) , DELETED(2);

    private int value;
}

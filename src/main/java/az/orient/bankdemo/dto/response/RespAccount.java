package az.orient.bankdemo.dto.response;

import az.orient.bankdemo.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RespAccount {
    private Long id;
    private String name;
    private String accountNo;
    private String iban;
    private Double amount;
    private String currency;
    private RespCustomer respCustomer;
}

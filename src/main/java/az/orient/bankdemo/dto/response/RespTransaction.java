package az.orient.bankdemo.dto.response;

import az.orient.bankdemo.dto.TokenDto;
import az.orient.bankdemo.entity.Account;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RespTransaction {
    private Long id;
    private String receiptNo;
    private RespAccount dtAccount;
    private String crAccount;
    private Double amount;
}

package az.orient.bankdemo.dto.request;

import az.orient.bankdemo.dto.TokenDto;
import az.orient.bankdemo.dto.response.RespAccount;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReqTransaction {
    private Long id;
    private String receiptNo;
    private RespAccount dtAccount;
    private String crAccount;
    private Double amount;
    private TokenDto tokenDto;
}

package az.orient.bankdemo.dto.request;

import az.orient.bankdemo.dto.TokenDto;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class ReqAccount {
    private Long accountId;
    private String name;
    private String accountNo;
    private String iban;
    private Double amount;
    private String currency;
    private Long customerId;
    private TokenDto tokenDto;
}

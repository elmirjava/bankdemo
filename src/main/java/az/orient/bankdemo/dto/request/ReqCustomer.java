package az.orient.bankdemo.dto.request;

import az.orient.bankdemo.dto.TokenDto;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ReqCustomer {
    private Long customerId;
    private String name;
    private String surname;
    private String address;
    private Date dob;
    private String phone;
    private String cif;
    private String seria;
    private TokenDto tokenDto;
}

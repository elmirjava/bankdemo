package az.orient.bankdemo.controller;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqTransaction;
import az.orient.bankdemo.dto.response.RespTransaction;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.entity.Transaction;
import az.orient.bankdemo.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/transaction")
public class TransactionController {
    private  final TransactionService transactionService;

    @PostMapping("GetTransactionList")
    public Response<List<RespTransaction>> getTransactionList(@RequestBody ReqTransaction reqTransaction){

        return transactionService.getTransactionListByAccountId(reqTransaction);
    }
    @PostMapping("AddTransaction")
    public Response addTransaction(@RequestBody ReqTransaction reqTransaction){

        return transactionService.addTransaction(reqTransaction);
    }
    @PutMapping("/UpdateTransaction")
    public Response updateTransaction(@RequestBody ReqTransaction reqTransaction){

        return transactionService.updateTransaction(reqTransaction);
    }
    @PutMapping("/DeleteTransaction")
    public Response deleteTransaction(@RequestBody ReqTransaction reqTransaction){

        return transactionService.deleteTransaction(reqTransaction);
    }
}

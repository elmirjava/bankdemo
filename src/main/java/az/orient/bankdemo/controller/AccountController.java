package az.orient.bankdemo.controller;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqCustomer;
import az.orient.bankdemo.dto.response.RespAccount;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("account")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @PostMapping("/GetAccountList")
    public Response<List<RespAccount>> getAccountListByCustomerId(@RequestBody ReqAccount reqAccount) {

        return accountService.getAccountListByCustomerId(reqAccount);
    }

    @PostMapping("/CreateAccount")
    public Response createAccount(@RequestBody ReqAccount reqAccount){

        return accountService.createAccount(reqAccount);
    }
    @PutMapping("/UpdateAccount")
    public Response updateAccount(@RequestBody ReqAccount reqAccount){
        return accountService.updateAccount(reqAccount);
    }
    @PutMapping("/DeleteAccount")
    public Response deleteAccount (@RequestBody ReqAccount reqAccount){
        return accountService.deleteAccount(reqAccount);
    }
}

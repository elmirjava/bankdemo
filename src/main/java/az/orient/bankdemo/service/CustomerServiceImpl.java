package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.TokenDto;
import az.orient.bankdemo.dto.request.ReqCustomer;
import az.orient.bankdemo.dto.response.RespCustomer;
import az.orient.bankdemo.dto.response.RespStatus;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.entity.Customer;
import az.orient.bankdemo.entity.UserToken;
import az.orient.bankdemo.enums.Enums;
import az.orient.bankdemo.exception.BankException;
import az.orient.bankdemo.exception.ExceptionConstants;
import az.orient.bankdemo.repository.CustomerRepository;
import az.orient.bankdemo.util.Utility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private final Utility utility;

    @Override
    public Response<List<RespCustomer>> getCustomerList(TokenDto tokenDto) {
        Response<List<RespCustomer>> response = new Response<>();
        try {
            UserToken userToken = utility.checkToken(tokenDto);
            if (Objects.isNull(userToken)) {
                throw new BankException(ExceptionConstants.INVALID_TOKEN, "Invalid token");
            }
            List<Customer> customerList = customerRepository.findAllByActive(Enums.ACTIVE.getValue());
            if (customerList.isEmpty()) {
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found!!!");
            }
            List<RespCustomer> responseCustomers = customerList.stream().map(customer ->
                    convert(customer)).collect(Collectors.toList());
            response.setT(responseCustomers);

            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<RespCustomer> getCustomerById(ReqCustomer reqCustomer) {
        Response<RespCustomer> response = new Response<>();
        try {
            UserToken userToken = utility.checkToken(reqCustomer.getTokenDto());
            if (Objects.isNull(userToken)) {
                throw new BankException(ExceptionConstants.INVALID_TOKEN, "Invalid Token");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(reqCustomer.getCustomerId(), Enums.ACTIVE.getValue());
            if (customer == null) {
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found!!!");
            }
            RespCustomer respCustomer = convert(customer);
            response.setT(respCustomer);
            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response addCustomer(ReqCustomer reqCustomer) {
        Response response = new Response();
        try {
            UserToken userToken = utility.checkToken(reqCustomer.getTokenDto());
            if (Objects.isNull(userToken)) {
                throw new BankException(ExceptionConstants.INVALID_TOKEN, "Invalid Token");
            }
            String name = reqCustomer.getName();
            String surname = reqCustomer.getSurname();
            if (name == null || surname == null) {
                throw new BankException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid request data");
            }
            Customer customer = Customer.builder()
                    .name(name)
                    .surname(surname)
                    .dob(reqCustomer.getDob())
                    .address(reqCustomer.getAddress())
                    .phone(reqCustomer.getPhone())
                    .cif(reqCustomer.getCif())
                    .seria(reqCustomer.getSeria())
                    .build();
            customerRepository.save(customer);
            response.setRespStatus(RespStatus.successMessage());

        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response updateCustomer(ReqCustomer reqCustomer) {
        Response response = new Response();
        try {
            UserToken userToken = utility.checkToken(reqCustomer.getTokenDto());
            if (Objects.isNull(userToken)) {
                throw new BankException(ExceptionConstants.INVALID_TOKEN, "Invalid Token");
            }
            Long customerId = reqCustomer.getCustomerId();
            String name = reqCustomer.getName();
            String surname = reqCustomer.getSurname();
            if (customerId == null || name == null || surname == null) {
                throw new BankException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid requet data");
            }

            Customer customer = customerRepository.findCustomerByIdAndActive(customerId, Enums.ACTIVE.getValue());

            if (customer == null) {
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found");
            }
            customer = Customer.builder()
                    .id(customer.getId())
                    .name(name)
                    .surname(surname)
                    .dob(reqCustomer.getDob())
                    .address(reqCustomer.getAddress())
                    .phone(reqCustomer.getPhone())
                    .cif(reqCustomer.getCif())
                    .seria(reqCustomer.getSeria())
                    .active(customer.getActive())
                    .dataDate(customer.getDataDate())
                    .build();
            customerRepository.save(customer);
            response.setRespStatus(RespStatus.successMessage());

        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response deleteCustomer(ReqCustomer reqCustomer) {
        Response response = new Response();
        try {
            UserToken userToken = utility.checkToken(reqCustomer.getTokenDto());
            if (Objects.isNull(userToken)) {
                throw new BankException(ExceptionConstants.INVALID_TOKEN, "Invalid Token");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(reqCustomer.getCustomerId(), Enums.ACTIVE.getValue());
            if (customer == null) {
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found");
            }
            customer.setActive(Enums.DEACTIVE.getValue());
            customerRepository.save(customer);
            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }

        return response;
    }

    private RespCustomer convert(Customer customer) {
        RespCustomer respCustomer = RespCustomer.builder().
                id(customer.getId()).
                name(customer.getName()).
                surname(customer.getSurname()).
                address(customer.getAddress()).
                dob(customer.getDob() != null ? df.format(customer.getDob()) : null).
                phone(customer.getPhone()).
                cif(customer.getCif()).
                seria(customer.getSeria()).
                build();
        return respCustomer;
    }
}

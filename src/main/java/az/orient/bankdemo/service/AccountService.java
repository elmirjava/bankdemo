package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqCustomer;
import az.orient.bankdemo.dto.response.RespAccount;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
public interface AccountService {

     Response<List<RespAccount>> getAccountListByCustomerId(ReqAccount reqAccount);


     Response createAccount(ReqAccount reqAccount);

     Response updateAccount(ReqAccount reqAccount);

    Response deleteAccount(ReqAccount reqAccount);
}

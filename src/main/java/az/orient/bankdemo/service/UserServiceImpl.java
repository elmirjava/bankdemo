package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.TokenDto;
import az.orient.bankdemo.dto.request.ReqUser;
import az.orient.bankdemo.dto.response.RespStatus;
import az.orient.bankdemo.dto.response.RespUser;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.entity.User;
import az.orient.bankdemo.entity.UserToken;
import az.orient.bankdemo.enums.Enums;
import az.orient.bankdemo.exception.BankException;
import az.orient.bankdemo.exception.ExceptionConstants;
import az.orient.bankdemo.repository.UserRepository;
import az.orient.bankdemo.repository.UserTokenRepository;
import az.orient.bankdemo.util.Utility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final UserTokenRepository userTokenRepository;

    private final Utility utility;
    @Override
    public Response login(ReqUser reqUser) {
        Response response=new Response();
        try{
            String username= reqUser.getUsername();
            String password=reqUser.getPassword();
            User user=userRepository.findUserByUsernameAndPasswordAndActive(username,password,Enums.ACTIVE.getValue());
            if(Objects.isNull(user)){
                throw new BankException(ExceptionConstants.USER_NOT_FOUND,"User Not Found");
            }
            String token= UUID.randomUUID().toString();
            UserToken userToken=new UserToken();
            userToken.setUser(user);
            userToken.setToken(token);
            userTokenRepository.save(userToken);
            TokenDto tokenDto=new TokenDto();
            tokenDto.setUserId(userToken.getId());
            tokenDto.setToken(userToken.getToken());
            RespUser respUser=new RespUser();
            respUser.setTokenDto(tokenDto);
            respUser.setFullName(user.getFullName());
            response.setT(respUser);
            response.setRespStatus(RespStatus.successMessage());
        }catch (BankException bankException){
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }

    @Override
    public Response logout(TokenDto tokenDto) {
        Response response=new Response();
        try{
           UserToken userToken= utility.checkToken(tokenDto);
           if(Objects.isNull(userToken)){
               throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid token");
           }
           userToken.setActive(Enums.DEACTIVE.getValue());
           userTokenRepository.save(userToken);
           response.setRespStatus(RespStatus.successMessage());
        }catch (BankException bankException){
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }
}

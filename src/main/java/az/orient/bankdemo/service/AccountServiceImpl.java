package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqCustomer;
import az.orient.bankdemo.dto.response.RespAccount;
import az.orient.bankdemo.dto.response.RespCustomer;
import az.orient.bankdemo.dto.response.RespStatus;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.entity.Account;
import az.orient.bankdemo.entity.Customer;
import az.orient.bankdemo.entity.UserToken;
import az.orient.bankdemo.enums.Enums;
import az.orient.bankdemo.exception.BankException;
import az.orient.bankdemo.exception.ExceptionConstants;
import az.orient.bankdemo.repository.AccountRepository;
import az.orient.bankdemo.repository.CustomerRepository;
import az.orient.bankdemo.util.Utility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final Utility utility;

    @Override
    public Response<List<RespAccount>> getAccountListByCustomerId(ReqAccount reqAccount) {
        Response<List<RespAccount>> response = new Response<>();
        try {
            UserToken userToken=utility.checkToken(reqAccount.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long customerId= reqAccount.getCustomerId();
            if(Objects.isNull(customerId)){
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND,"Customer not found");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(customerId, Enums.ACTIVE.getValue());
            if (customer == null) {
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found");
            }
            List<Account> accountList = accountRepository.findAllByCustomerAndActive(customer, Enums.ACTIVE.getValue());
            if (accountList.isEmpty()) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            List<RespAccount> respAccountList=accountList.stream().map(this::convert).collect(Collectors.toList());
               response.setT(respAccountList);
               response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response createAccount(ReqAccount reqAccount) {
        Response response= new Response<>();
        try{
            UserToken userToken=utility.checkToken(reqAccount.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long customerId=reqAccount.getCustomerId();
            String name= reqAccount.getName();
            if(customerId==null||name==null){
                throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND,"Customer not found");
            }
            Customer customer=customerRepository.findCustomerByIdAndActive(customerId,Enums.ACTIVE.getValue());
             if(customer==null){
                 throw new BankException(ExceptionConstants.CUSTOMER_NOT_FOUND,"Customer not found");
             }
             Account account=Account.builder()
                     .name(reqAccount.getName())
                     .accountNo(reqAccount.getAccountNo())
                     .iban(reqAccount.getIban())
                     .amount(reqAccount.getAmount())
                     .currency(reqAccount.getCurrency())
                     .customer(customer)
                     .build();
             accountRepository.save(account);
             response.setRespStatus(RespStatus.successMessage());
        }catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response updateAccount(ReqAccount reqAccount) {
        Response response=new Response();
        try{
            UserToken userToken=utility.checkToken(reqAccount.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long accountId= reqAccount.getAccountId();
            if(accountId==null){
                throw new BankException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid request data");
            }
            Account account = accountRepository.findAccountByIdAndActive(accountId, Enums.ACTIVE.getValue());
            if (account==null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Customer customer=customerRepository.findCustomerByIdAndActive(accountId,Enums.ACTIVE.getValue());
            account=Account.builder().
                    id(account.getId()).
                    name(reqAccount.getName()).
                    accountNo(reqAccount.getAccountNo()).
                    iban(reqAccount.getIban()).
                    amount(reqAccount.getAmount()).
                    currency(reqAccount.getCurrency()).
                    dataDate(account.getDataDate()).
                    active(account.getActive()).
                    customer(customer).
                    build();
            accountRepository.save(account);
            response.setRespStatus(RespStatus.successMessage());

        }catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response deleteAccount(ReqAccount reqAccount) {
        Response response=new Response();
        try{
            UserToken userToken=utility.checkToken(reqAccount.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long accountId=reqAccount.getAccountId();
            if(accountId==null){
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND,"Account not found");
            }
            Account account=accountRepository.findAccountByIdAndActive(accountId,Enums.ACTIVE.getValue());
            if(account==null){
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND,"Account not found");
            }
            account.setActive(Enums.DEACTIVE.getValue());
            accountRepository.save(account);
            response.setRespStatus(RespStatus.successMessage());
        }catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    public RespAccount convert(Account account) {
        RespCustomer respCustomer=RespCustomer.builder()
                .id(account.getCustomer().getId())
                .name(account.getCustomer().getName())
                .surname(account.getCustomer().getSurname())
                .build();
        RespAccount respAccount = RespAccount.builder()
                .id(account.getId())
                .name(account.getName())
                .accountNo(account.getAccountNo())
                .iban(account.getIban())
                .amount((double) account.getAmount()/100)
                .currency(account.getCurrency())
                .respCustomer(respCustomer)
                .build();
        return respAccount;
    }

}

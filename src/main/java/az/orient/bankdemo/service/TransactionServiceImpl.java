package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqTransaction;
import az.orient.bankdemo.dto.response.*;
import az.orient.bankdemo.entity.Account;
import az.orient.bankdemo.entity.Transaction;
import az.orient.bankdemo.entity.UserToken;
import az.orient.bankdemo.enums.Enums;
import az.orient.bankdemo.exception.BankException;
import az.orient.bankdemo.exception.ExceptionConstants;
import az.orient.bankdemo.repository.AccountRepository;
import az.orient.bankdemo.repository.TransactionRepository;
import az.orient.bankdemo.util.Utility;
import jdk.jfr.Frequency;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    private  final Utility utility;
    @Override
    public Response<List<RespTransaction>> getTransactionListByAccountId(ReqTransaction reqTransaction) {
        Response<List<RespTransaction>> response = new Response<>();
        try {
            UserToken userToken=utility.checkToken(reqTransaction.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long accountId= reqTransaction.getDtAccount().getId();
            if (accountId == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Account account = accountRepository.findAccountByIdAndActive(accountId, Enums.ACTIVE.getValue());
            if (account == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            List<Transaction> transactionList = transactionRepository.findAllByDtAccountAndActive(account, Enums.ACTIVE.getValue());
            if (transactionList.isEmpty()) {
                throw new BankException(ExceptionConstants.TRANSACTION_NOT_FOUND, "Transaction not found");
            }
            List<RespTransaction> respTransactionList = transactionList.stream().map(this::convert).collect(Collectors.toList());
            response.setT(respTransactionList);
            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response addTransaction(ReqTransaction reqTransaction) {
        Response response = new Response<>();
        try {
            UserToken userToken=utility.checkToken(reqTransaction.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long accountId = reqTransaction.getDtAccount().getId();
            String receiptNo = reqTransaction.getReceiptNo();
            if (accountId == null || receiptNo == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Account account = accountRepository.findAccountByIdAndActive(reqTransaction.getDtAccount().getId(), Enums.ACTIVE.getValue());
            if (account == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Transaction transaction = Transaction.builder()
                    .receiptNo(reqTransaction.getReceiptNo())
                    .dtAccount(account)
                    .crAccount(reqTransaction.getCrAccount())
                    .amount((double) reqTransaction.getAmount() / 100)
                    .build();
            response.setT(transaction);
            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception e) {
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response updateTransaction(ReqTransaction reqTransaction) {
        Response response = new Response();
        try {
            UserToken userToken=utility.checkToken(reqTransaction.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long accountId = reqTransaction.getDtAccount().getId();
            if (accountId == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Transaction transaction = transactionRepository.findTransactionByIdAndActive(accountId, Enums.ACTIVE.getValue());
            if (transaction == null) {
                throw new BankException(ExceptionConstants.TRANSACTION_NOT_FOUND, "Transaction not found");
            }
            Account account = accountRepository.findAccountByIdAndActive(accountId, Enums.ACTIVE.getValue());
            if (account == null) {
                throw new BankException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
             transaction=Transaction.builder().
                     id(transaction.getId()).
                     receiptNo(reqTransaction.getReceiptNo()).
                     dtAccount(account).
                     crAccount(reqTransaction.getCrAccount()).
                     amount(reqTransaction.getAmount()).
                     trDate(transaction.getTrDate()).
                     active(transaction.getActive()).
                     build();
            response.setT(transaction);
            response.setRespStatus(RespStatus.successMessage());
        } catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception exception) {
            exception.printStackTrace();
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response deleteTransaction(ReqTransaction reqTransaction) {
        Response response=new Response();
        try{
            UserToken userToken=utility.checkToken(reqTransaction.getTokenDto());
            if(Objects.isNull(userToken)){
                throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid Token");
            }
            Long transactionId= reqTransaction.getId();
            if(transactionId==null){
                throw new BankException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid request data");
            }
            Transaction transaction=transactionRepository.findTransactionByIdAndActive(transactionId,Enums.DEACTIVE.getValue());
            if(transaction==null){
                throw new BankException(ExceptionConstants.TRANSACTION_NOT_FOUND,"Transaction not found");
            }
            transactionRepository.save(transaction);
            response.setRespStatus(RespStatus.successMessage());
        }catch (BankException bankException) {
            bankException.printStackTrace();
            response.setRespStatus(new RespStatus(bankException.getCode(), bankException.getMessage()));
        } catch (Exception exception) {
            exception.printStackTrace();
            response.setRespStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    private RespTransaction convert(Transaction transaction) {
        RespCustomer respCustomer = RespCustomer.builder()
                .id(transaction.getDtAccount().getCustomer().getId())
                .name(transaction.getDtAccount().getCustomer().getName())
                .surname(transaction.getDtAccount().getCustomer().getSurname())
                .build();
        RespAccount respAccount = RespAccount.builder()
                .id(transaction.getDtAccount().getId())
                .name(transaction.getDtAccount().getName())
                .accountNo(transaction.getDtAccount().getAccountNo())
                .iban(transaction.getDtAccount().getIban())
                .amount((double) transaction.getDtAccount().getAmount() / 100)
                .currency(transaction.getDtAccount().getCurrency())
                .respCustomer(respCustomer)
                .build();
        RespTransaction respTransaction = RespTransaction.builder()
                .id(transaction.getId())
                .receiptNo(transaction.getReceiptNo())
                .dtAccount(respAccount)
                .crAccount(transaction.getCrAccount())
                .amount((double) transaction.getAmount() / 100)
                .build();

        return respTransaction;
    }
}


package az.orient.bankdemo.service;

import az.orient.bankdemo.dto.request.ReqAccount;
import az.orient.bankdemo.dto.request.ReqTransaction;
import az.orient.bankdemo.dto.response.RespTransaction;
import az.orient.bankdemo.dto.response.Response;
import az.orient.bankdemo.entity.Transaction;

import java.util.List;

public interface TransactionService {
    Response<List<RespTransaction>> getTransactionListByAccountId(ReqTransaction reqTransaction);

    Response addTransaction(ReqTransaction reqTransaction);

    Response updateTransaction(ReqTransaction reqTransaction);

    Response deleteTransaction(ReqTransaction reqTransaction);
}

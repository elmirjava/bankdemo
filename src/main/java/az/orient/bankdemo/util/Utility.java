package az.orient.bankdemo.util;

import az.orient.bankdemo.dto.TokenDto;
import az.orient.bankdemo.entity.User;
import az.orient.bankdemo.entity.UserToken;
import az.orient.bankdemo.enums.Enums;
import az.orient.bankdemo.exception.BankException;
import az.orient.bankdemo.exception.ExceptionConstants;
import az.orient.bankdemo.repository.UserRepository;
import az.orient.bankdemo.repository.UserTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class Utility {
    private final UserRepository userRepository;
    private final UserTokenRepository userTokenRepository;

    public UserToken checkToken(TokenDto tokenDto) {
        String token= tokenDto.getToken();
        Long userId= tokenDto.getUserId();
        if(Objects.isNull(token)||Objects.isNull(userId)){
            throw new BankException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid request data");
        }
        User user=userRepository.findUserByIdAndActive(userId, Enums.ACTIVE.getValue());
        if(Objects.isNull(user)){
            throw new BankException(ExceptionConstants.USER_NOT_FOUND,"User not found");
        }
        UserToken userToken=userTokenRepository.findUserTokenByUserAndTokenAndActive(user,token,Enums.ACTIVE.getValue());
        if(Objects.isNull(userToken)){
            throw new BankException(ExceptionConstants.INVALID_TOKEN,"Invalid token");
        }
        return userToken;
    }
}
